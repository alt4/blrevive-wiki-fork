---
title: Join Servers
author: BLRevive
---
# :material-play: Joining Servers

BLRevive provides a few community hosted servers which is the easiest way to get into a game.
You can also add other servers and connect to them.

## Join Community Server

The easiest way to get into a game is joining one of our community hosted servers.


1. open the `Launcher` tab
1. select `Server List`
1. choose one of the listed servers and click on the `Connect` button at the bottom

## Join Custom Server

You can also join servers hosted from other users by adding the server to your server list.

1. open the `Launcher` tab
1. select `Server List`
1. click on `Add Server`
    - a new server entry is added to the end of your server list
1. click on the ![ServerConfigureButton] button of the new server entry
1. change `ServerAddress` to the address of the server you want to connect to
    > You don't need to change the ports unless the server owner told you to!
1. close the server configuration window
1. click on the `Connect` button of the server entry you added

[Discord]: https://discord.gg/wwj9unRvtN
[LauncherTab]: ./launcher_tab.png
[ServerList]: ./server_list.png
[ServerConnectButton]: ./server_connect_button.png
[AddServerButton]: ./add_server_button.png
[ServerConfigureButton]: ./configure_server_button.png