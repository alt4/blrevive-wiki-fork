---
title: Install
description: Guide how to install and configure BLRevive.
---

# BLRedit

BLRedit is an application developed by our community which allows playing BL:R by patching the client.

<div class="grid cards" markdown>

- :material-check-all: **advantages**

    ---

    - **unlimited loadouts**: supports unlimited loadouts.
    - **offline/lan server**: create and access your own local game servers easily.
    - **advanced loadout stats**: provides very detailed stats of your loadouts which the original game UI does not.

- :material-close: **downsides**

    ---

    - **requires additional software**: BLRedit patches the BL:R client to fix a bug which prevents joining game servers and integrate several features like loadout customization.
    - **device dependant**: options like settings, loadouts and such are saved *locally*. If you want to use the same configuration accross different devices you need to manually transfer and sync configuration files.
</div>

## :package: Installation

1. Download [BLREdit](https://github.com/HALOMAXX/BLREdit/releases) (**you only need the BLRedit.zip file**)
    - [direct download link of latest release](https://github.com/HALOMAXX/BLREdit/releases/download/latest/BLREdit.zip)
1. Extract `BLREdit.zip` to your preferred location (we suggest to use your documents directory but it is not mandatory)
1. Start `BLRedit.exe`
1. BLREdit will prompt for elevated permissions (this is only required at first start, see notice below)
    - click "Yes" to continue with Web-App-Links
    - click "No" to continue without Web-App-Links
    !!! info "Web-App-Links"
        BLREdit supports a feature called Web-App-Protocol which allows you to join servers by simply clicking on a link in web browser but it needs
        admin privileges to set this up.
        If you do not want this feature you can decline the elevation prompt and disable the Web-API options in the settings later (if you do not disable this BLREdit will keep ask for permissions).
        
1. A message box appears saying that BLREdit will download missing files (those are required to run BLREdit). 
    - click "OK" to confirm and wait for download to be finished

After the files downloaded BLREdit will restart and you need to configure it (follow steps below).

## :wrench: Configure BLREdit

BLREdit will prompt you to

1. add a game client
1. change your player name

Click on "OK" to continue which brings you to the settings tabs.

### Set Playername

1. replace *BLREdit-Player* in the `Player Name` textbox with your desired player name


### Add Game Client

1. head to the `Launcher` tab
1. select `Client List`
1. click on `Add Client` (at the bottom of the window)
1. select `[BLR]\Binaries\Win32\FoxGame-win32-Shipping.exe` and confirm

## :tada: Next Steps

After you finished installing and configuring BLREdit you probably want to customize your loadouts and join or host servers.
Here is a list of instructions you most likely want to read:

- [:gun: How to customize your loadouts](../blredit/loadout-customization.md)
- [:triangular_flag_on_post: How to join community servers](../blredit/join-server.md)
- [:rocket: How to host your own server](../hosting/game-server/getting-started.md)

## :book: FAQ

Before starting to seek help in our [Discord] make sure it isn't covered by this FAQ!

!!! question "Client crashes when loading"
    Most likely you forgot to patch the client. If you did and it does still crash, seek help in our [Discord] or open an issue.


!!! question "Can't connect to steam"
    Your client failed to connect to the server. Either try a different server or report it to the server owner.


[Discord]: https://discord.gg/wwj9unRvtN
[LauncherTab]: ../blredit/launcher_tab.png
[ClientList]: ../blredit/client_list.png
[AddClientButton]: ../blredit/add_client_button.png