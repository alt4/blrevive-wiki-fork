---
title: Manage Loadouts
author: BLRevive
---

# :material-tank: Manage loadouts

Managing loadouts in BLRedit is pretty easy!

## Add loadout

1. click on the `Add Loadout` button in the top left corner
1. change "New Loadout" to your desired loadout name

    !!! tip "Loadout names"
        The loadout will appear under that name in the ingame loadout list (where you can select your loadout).
        You can change the name at any time in the `Loadouts` tab.

1. customize your loadout

## Enable/Disable loadout

You can enable and disable specific loadouts which is useful if you have a lot of it!

1. go to the `Loadouts` tab
1. click on the `Apply` button on the top right corner of the loadout to toggle it

    !!! info
        - if the `Apply` button is **orange**{.orange} the loadout is **enabled**, making it appear in the ingame loadout list
        - if the `Apply` button is **grey**{.grey}, the loadout is **disabled**, hiding it from the ingame loadout list

1. click on `Update Loadouts` in the top right corner to update your loadouts ingame


## Modify loadouts

You can modify your loadouts (remove or change the name) in the `Loadouts` tab.

# FAQ

??? question "My loadout does not appear ingame"
    Try clicking the `Update Loadouts` button at the top left corner - this will update your loadouts while being ingame.
    The next time you die ingame the loadout should appear in the list, if not seek help in our [#support] channel on discord.
