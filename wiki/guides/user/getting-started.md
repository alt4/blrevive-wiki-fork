---
title: Install
author: BLRevive
---

Welcome to the world of BLRevive, a place for all Blacklight refugees! This guide should help you install and configure Blacklight: Retribution to play on PC again.

!!! tip ":simple-discord: Join our [BLRevive Discord] server!"

    Beside our great and friendly community we prepared plenty of resources and tools for you, such as..

    === ":material-account-multiple-plus-outline: Finding Players"
        Use the **@LFG** ping in the [#lfg] channel to locate other players when the servers are quiet. This ensures you always have a team ready for action.

    === ":material-calendar-sync: Recurring Events"
        Stay informed about upcoming events that promise bustling lobbies. These events are your best bet for finding full games and engaging in epic battles.

    === ":material-help-circle: Support"
        Encountered an issue? Head over to the [#support] channel to seek assistance. Our community and support team are ready to help you resolve any problems swiftly.

    === ":material-folder-star-multiple-outline: Resources"
        Discover additional resources such as weapon builds in the [#build-share] channel and other valuable information in the [#resources] channel. Equip yourself with the best strategies and gear to dominate the game.
    
    === ":material-shield-bug: Bug Reports"
        Found a bug ? Tell us about it in [#bug-reports] to help us improve our services!
    
    ...and even more, [we are awaiting you Agent][BLRevive Discord]!

## :material-download: Download BL:R
If BL:R is available in your steam library (because you added it before the PC servers shutdown) you should install it from there!
Otherwise use one of the download links listed in [here](#download-links).

=== ":material-steam: Steam"
        
    1. Open the steam client
    1. Go to `Library` tab
    1. Right-click `Blacklight: Retribution` and select `Install`

    !!! info
        The guide refers to the installation directory of BL:R as `[BLR]`, which in this case is 
        
        - Windows: `C:\Program Files (x86)\Steam\steamapps\common\blacklightretribution`
        - Linux: `~/.local/share/Steam/steamapps/common/blacklightretribution`

=== ":material-download: Download Links"

    1. Download [blacklightretribution.zip] (*if not available choose another link from the list below*)
    1. Extract the archive to a folder of your preference
        
    !!! info
        The guide refers to this folder as `[BLR]`. So if you extracted it to `C:\Program Files(x86)\BLR`, `[BLR]/Binaries/Win32` means `C:\Program Files(x86\BLR\Binaries\Win32`.

    | Source | Direct Link | Note |
    |---|---|---|
    | DDD | [blacklightretribution.zip] | recommended because very fast |
    | [Archive.org](https://archive.org/details/blacklightretribution-steam-untouched.7z) | [blacklightretribution-steam-untouched.7z](https://archive.org/download/blacklightretribution-steam-untouched.7z/blacklightretribution-steam-untouched.7z) | **extremely slow, try other downloads before using this** |


??? note "Playing on :simple-linux: Linux / :simple-apple: MacOS"

    It is possible to play BLR on Linux/MacOS even tho it was not officially supported.

    === ":material-steam: Steam Play"

        Steam has it's own wine layer called proton which makes it easy to setup the BLR client on linux!
        You can find more informations about Steam Play [here](https://steamcommunity.com/games/221410/announcements/detail/1696055855739350561?snr=100601___).

        1. Open steam client
        1. Navigate to `Steam` -> `Settings` -> `Compatibility` and check `Enable Steam Play for all other titles`
        1. Follow [Download Blacklight: Retribution](./BLR.md)

    === ":simple-wine: Wine"

        The BLR client runs in [wine] without additional setup. 

        !!! warning "ZCure not supported with wine"

            ZCure relies on the steam integration which is not available in wine (unless you also install the steam client in wine itself).
            It is recommended to go with [Steam Play](#steam-play)!

        1. Install [wine]
        1. Open a shell in `[BLR]/Binaries/Win32`
        1. Start BLR client with wine
            ``` bash
            wine ./FoxGame-win32-Shipping.exe
            ```

    [blacklightretribution.zip]: https://chnt.pw/blre

## :material-cog: Configure 

There are currently two methods which allow to play BL:R again. They are compatible with each other, meaning you can use both methods at the same time without conflicts.

- Go with [ZCure](ZCure.md) if you just want to play BL:R without much hassle
- Go with [BLRedit](BLRedit.md) if you enjoy fine-graded loadout details or want to host offline/lan servers

If you are still uncertain take a look at the [Feature Comparison](#feature-comparison) table to decide which method is the best fit for you.

## Feature Comparison

This table should help you decide which method to choose.

| Feature | ZCure | BLRedit |
|---|---|---|
| Vanilla Client Support | :material-check:{.green} | :material-close:{.red} *(requires patching the game client)* |
| Loadout / Character Customization | :material-check:{.green} *(50 slots)* | :material-check-all:{.green} *(unlimited)* |
| Local / LAN Game Servers | :material-close:{.red} | :material-check:{.green} |
| Community Servers | :material-check:{.green} | :material-check:{.green} |
| Steam Integration | :material-check:{.green} | :material-close:{.red} |
| Balance Patches | :material-close:{.red} | :material-check:{.green} |
| Available for Linux | :material-check:{.green} | :material-close:{.red} |

*[Vanilla Client Support]: no tools/software required to play; use the untouched client from steam
*[Loadout / Character Customization]: Customize your weapons and character
*[Local / LAN Game Servers]: Host your own game server for offline / lan games
*[Community Servers]: Join our community servers
*[Steam Integration]: Use your steam account to login and manage your profile/loadouts
*[Balance Patches]: Support for community balance patches (eg weapon balancing)
