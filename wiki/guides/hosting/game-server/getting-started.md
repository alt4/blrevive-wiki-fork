---
title: Hosting
author: BLRevive
---
# Game Server

The game server we use is actually the replication server which comes with UE3 by default and luckily is provided by the latest BL:R clients. BLRevive extends its capacities with its custom modding framework.

## Common

Those things are essential to get started, whatever the platform:

* If you want your server to be available publicly, you'll need [a public IPv4 address](http://ifconfig.me/) that you can do port-forwarding on
	* You'll need to forward and create firewall rules for BL:R's executable `FoxGame-win32-Shipping.exe` (default port is `7777` UDP (game) and TCP (api))
* You'll need the latest version (v3.02) of the game files.

### Downloading Game Client (using DepotDownloader)

!!! warning "Requires BL:R in your Steam Library"
	If you don't have BL:R in yout steam library, use the game client provided in [here](./../../user/getting-started.md#download-links). 
	The benefit of using DepotDownloader is reduced disk size because it only downloads files necessary for the server to run.
	
	This guide assumes you placed the game client files in this location:
	
	- Windows: `C:\Users\[User]\Documents\blacklightre\client\3.02`
	- Linux: `/srv/blacklightre/client/3.02`

	Which is referred as *client folder* (`[client]`) from now on.

The necessary game files can be acquired using [DepotDownloader](https://github.com/SteamRE/DepotDownloader).

=== "Linux"
    ```bash title="Download BLR client with DepotDownloader"
    STEAM_USERNAME=[replace with your username]
	# Adjust CLIENT_DIR if you want a different destination dir
	CLIENT_DIR=/srv/blacklightre/client/3.02
	
	# Download and extract DepotDownloader
    curl -LO https://github.com/SteamRE/DepotDownloader/releases/latest/download/DepotDownloader-linux-x64.zip
    unzip DepotDownloader-linux-x64.zip

	# Download filelist
    curl -LO https://gitlab.com/-/snippets/2529720/raw/main/filelist.txt

	# create destination dir if necessary
	mkdir -p $CLIENT_DIR

	# Download BLR (make sure to adjust the path if needed)
    chmod +x DepotDownloader
    ./DepotDownloader -app 209870 -depot 209871 -manifest 8740515310020658800 -username "$STEAM_USERNAME" -dir "$CLIENT_DIR" -filelist filelist.txt
    ```

	The client is downloaded to `/srv/blacklightre/client/3.02` (change the `CLIENT_DIR` if you want another destination directory).

=== "Windows"
	```powershell title="Download BLR client with DepotDownloader in PowerShell"
	$STEAM_USERNAME = "[replace with your username]"
	$CLIENT_DIR = Resolve-Path "~\Documents\blacklightre\client\3.02"

	# Download and extract DepotDownloader
	Invoke-WebRequest -Uri "https://github.com/SteamRE/DepotDownloader/releases/download/latest/DepotDownloader-windows-x64.zip" -OutFile "DepotDownloader.zip"

	# Unzip DepotDownloader
	Expand-Archive -Path "DepotDownloader.zip" -DestinationPath "."

	# Download filelist.txt
	Invoke-WebRequest -Uri "https://gitlab.com/-/snippets/2529720/raw/main/filelist.txt" -OutFile "filelist.txt"

	# Create client folder if necessary
	New-Item -Path $CLIENT_DIR -ItemType Directory -Force

	# Download BLR client (make sure to adjust the path if needed)
	Start-Process -FilePath "./DepotDownloader" -ArgumentList "-app 209870 -depot 209871 -manifest 8740515310020658800 -username $STEAM_USERNAME -dir $CLIENT_DIR -filelist filelist.txt"
	```

	The client is downloaded to `C:\Users\[YourUsername]\Documents\blacklightre\client\3.02` (change the `CLIENT_DIR` if you want another destination directory).

## Setup Game Server

Use the instructions specific to the platform you want to run the game server on:

- [Container (Docker/Podman)](./container.md)
- [Windows](./windows.md)
- [Linux](./linux.md)

## Configure Game Server

Follow all the instructions in *Configuration* one by one:

1. [Artemis](./artemis-config.md)
2. [BLRevive](./blrevive-config.md)
3. [Server Utils](./server-utils-config.md)