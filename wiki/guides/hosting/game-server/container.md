For deploying game servers using containerization we provide the [northamp/docker-blrevive] image which utilizes the [northamp/artemis-blrevive] launcher.

!!! info
    If you created configurations for other container stacks (ie. Kubernetes) we are happily adding them here.
    Just reach out to us in the [#resources] channel on discord or by creating a merge request on the [blrevive/wiki] repo!

## Docker Compose

### Prerequisites

- an OCI compatible container runtime ([Docker](https://docker.io) or [Podman](https://podman.io))
- [Docker Compose]
    
    !!! tip "Podman also supports docker compose!"

### Setup

1. Create the directory `/srv/blacklightre/config` and configure BLRevive & server-utils as you see fit - see [Configuration/BLRevive] and [Configuration/Server Utils]
1. Create the following docker-compose file in `/srv/blacklightre/docker-compose.yml`:

    ```yaml title="/srv/blacklightre/docker-compose.yml"
    version: '3'
    services:
    blrevive:
        image: registry.gitlab.com/northamp/docker-blrevive:latest
        container_name: blrevive
        restart: always
        environment:
            # change the server name to your liking, use + instead of spaces!
            - BLREVIVE_GAMESETTINGS_SERVERNAME=[Earth]+Yet Another+BLRE+Server
            - BLREVIVE_GAMESETTINGS_PLAYLIST=TDM
            - BLREVIVE_GAMESETTINGS_CONFIGNAME=myserver
        volumes:
            # adjust the paths accordingly if using different structure!
            - /srv/blacklightre/client/3.02:/mnt/blacklightre/game:ro
            - /srv/blacklightre/config:/mnt/blacklightre/config
            # the mounts below are optional if you want to use custom binaries for blrevive/modules
            # - /srv/blacklightre/blre:/mnt/blackightre/blre:ro
            # - /srv/blacklightre/modules:/mnt/blacklightre/modules:ro
        ports:
            - 7777:7777/udp
            - 7777:7777/tcp
    ```

1. Spin up a server with `docker compose up -d`, it should then start successfully (which you can confirm by running `docker compose logs` and/or `docker ps -a`)

### Custom BLRevive / Modules 

By default the latest versions of BLRevive and the server-utils are bundled within the container image. 

You can provide your own binaries for BLRevive and/or modules. This useful if you want to use additional modules or for debugging.

| mount point | description | expected files |
|---|---|---|
| `/mnt/blacklightre/blre` | contains the BLRevive library | - [`BLRevive.dll`][BLRevive/Releases] <br> - [`DINPUT8.dll`][BLRevive/Releases] |
| `/mnt/blacklightre/modules` | contains the BLRevive modules (look into  [BLRevive/Modules] for available modules) | - [`server-utils.dll`][ServerUtils/Releases] <br> - *additional modules* |

For example if you want to add another module place it in `/srv/blacklightre/modules` add the mount point to your `docker-compose.yml`:

```yaml
volumes:
    - /srv/blacklightre/modules:/mnt/blacklightre/modules:ro
```

[Docker Compose]: https://docs.docker.com/compose/
[blrevive/wiki]: https://gitlab.com/blrevive/wiki
[Configuration/BLRevive]: ./blrevive-config.md
[Configuration/Server Utils]: ./server-utils-config.md
[BLRevive/Releases]: https://gitlab.com/blrevive/blrevive/-/releases
[ServerUtils/Releases]: https://gitlab.com/blrevive/modules/server-utils/-/releases
[BLRevive/Modules]: https://gitlab.com/blrevive/modules
[northamp/docker-blrevive]: https://gitlab.com/northamp/docker-blrevive
[northamp/artemis-blrevive]: https://gitlab.com/northamp/artemis-blrevive