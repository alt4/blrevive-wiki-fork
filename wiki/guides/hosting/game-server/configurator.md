<h2>Server Configuration</h2>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
<style>
    details { 
        display: flex; 
        flex-direction: column; 
        -webkit-user-select: none; /* Safari */        
        -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* IE10+/Edge */
        user-select: none; /* Standard */
    }
    details label { display:flex; justify-content: space-between; padding:0.2rem 1rem;}
    input { border: 1px solid rgba(250, 80, 0, 0.3); }

    details label:hover { background-color: rgba(250, 80, 0, 0.1); }
</style>

!!! danger "Currently not fully working, dont use for production!"

<div id="serverConfig"></div>
<a class="md-button md-button--primary" onclick="copyConfigToClipboard('serverConfigForm')">Copy to Clipboard</a>

<h2>Playlist</h2>
<details class="abstract" open>
    <summary>Playlist Entries</summary>
    <div id="playlistEntries"></div>
    <button class="md-button md-button--primary" onclick="addPlaylistEntry()">Add Entry</button>
</details>
<button class="md-button md-button--primary" onclick="copyPlaylistToClipboard()">Copy to Clipboard</button>

<h3>Generated Playlist JSON</h3>
<pre id="playlistJsonOutput"></pre>
<a class="md-button md-button--primary" typ="button" id="downloadBtn">Download JSONs as ZIP</a>

<script>
    let playlist = [];

    const gameModes = {
        dm: 'DM',
        tdm: 'TDM',
    };

    const maps = {
        helodeck: 'HeloDeck',
        containment: 'Containment'
    }

    const serverConfigTemplate = [
        {
            title: "Server Settings",
            type: "info",
            properties: {
                ServerName: { name: "Server Name", default: ""},
                Map: { name: "Map", default: "helodeck", opts: maps},
                GameMode: { name: "Game Mode", default: "dm", opts: gameModes}
            }
        },
        {
            title: "Game Settings",
            type: "success",
            properties: {
                TimeLimit: { name: "Time Limit (minutes)", default: 10},
                GoalScore: { name: "Goal Score", default: 3000},
                GameRespawnTime: {name: "Respawn Time", default: 10.0, step: 0.1},
                GameForceRespawnTime: { name: "Forced Respawn Time", default: 30.0, step: 0.1 },
                GameSpectatorSwitchDelayTime: { name: "Spectator Switch Delay", default: 120.0, step: 0.1},
            }
        },
        {
            title: "Player & Bot Settings",
            type: "abstract",
            properties: {
                NumBots: { name: "Number of Bots", default: 0},
                MaxPlayers: { name: "Max Players", default: 16},
                ForceBotsToSingleTeam: { name: "Force Bots to One Team", default: false},
                AllowUnbalancedTeams: { name: "Allow Unbalanced Teams", default: false},
                RandomBotNames: { name: "Bot Names (separated with ,)", default: ""}
            }
        },
        {
            title: "Vote Kick Settings",
            type: "danger",
            properties: {
                NumEnemyVotesRequiredForKick: { name: "Enemy Votes Required for Kick", default: 4},
                NumFriendlyVotesRequiredForKick: { name: "Friendly Votes Required for Kick", default: 2},
                VoteKickBanSeconds: { name: "Vote Kick Ban Duration (seconds)", default: 1600}
            }
        },
        {
            title: "Mutators",
            type: "example",
            properties: {
                DisableDepots: { name: "Disable Depots", default: false },
                DisableHRV: { name: "Disable HRV", default: false },
                DisableHeadShots: { name: "Disable Headshots", default: false },
                StockLoadout: { name: "Stock Loadout Only", default: false },
                DisablePrimaries: { name: "Disable Primary Weapons", default: false },
                DisableSecondaries: { name: "Disable Secondary Weapons", default: false },
                DisableGear: { name: "Disable Gear", default: false },
                DisableTacticalGear: { name: "Disable Tactical Gear", default: false },
                DisableHealthRegen: { name: "Disable Health Regen", default: false },
                DisableElementalAmmo: { name: "Disable Elemental Ammo", default: false },
                HeadshotsOnly: { name: "Headshots Only", default: false },
                StaminaModifier: { name: "Stamina Modifier", default: 1.0, step: 0.1},
                HealthModifier: { name: "Health Modifier", default: 1.0, step: 0.1},
            }
        },
        {
            title: "Lobby Settings",
            type: "note",
            properties: {
                MinRequiredPlayersToStart: { name: "Minimum Players Required to Start Game", default: 2},
                PlayerSearchTime: { name: "Player Search Time", default: 30.0, step: 0.1 }
            }
        }
    ];

    function generateSelectHTML(name, opts) {
        let optsHTML = [];
        for(const [key, name] of Object.entries(opts)) {
            optsHTML.push(`
                <option value="${key}">${name}</option>
            `);
        }

        return `<select name="${name}">${optsHTML.join()}</select>`;
    }

    const gameModeSelectHTML = generateSelectHTML('GameMode', gameModes);
    const mapSelectHTML = generateSelectHTML('Map', maps);

    function generateServerConfigForm(id, isMain = false) {
        let formHTML = `<form id="${id}" class="serverConfigForm">`;

        for(const section of serverConfigTemplate) {
            if(!isMain && section.title === 'Server Settings') {
                continue;
            }

            formHTML += `<details class="${section.type}" ${isMain == true ? 'open' : ''}>`;
            formHTML += `<summary>${section.title}</summary>`;
            for(const [key, opts] of Object.entries(section.properties)) {
                formHTML += `<label>${opts.name}: `;
                if(opts.opts) {
                    formHTML += generateSelectHTML(key, opts.opts);
                } else if (Number.isInteger(opts.default)) {
                    formHTML += `<input name="${key}" type="number" value="${opts.default}" `;
                    if(opts.step) {
                        formHTML += ` step="${opts.step}"`;
                    }
                    formHTML += `>`;
                } else if (opts.default === true || opts.default === false) {
                    formHTML += `<input type="checkbox" name="${key}" ${opts.default ? 'checked' : ''}>`
                } else {
                    formHTML += `<input type="text" name="${key}" value="${opts.default}">`;
                }

                formHTML += `</label>`
            }
            formHTML += `</details>`
        }

        formHTML += `</form>`;
        return formHTML;
    }

    function getProp(formId, propName, def) {
        let el = document.querySelector(`#${formId} [name="${propName}"]`);
        if ( el == null) {
            return def;
        }
        if (el.type === "select-one") {
            return el.value;
        }
        if (el.type === 'number') {
            if(el.hasAttribute('step')) {
                return parseFloat(el.value).toFixed(5);
            } else {
                return parseInt(el.value);
            }
        } else if(el.type === 'checkbox') {
            return el.checked;
        } else if(el.type === 'text') {
            return el.value;
        }
    }

    function generateConfig(id) {
        let config = {
            ServerName: getProp(id, "ServerName", "BLRE Server"),
            properties: {
                GameRespawnTime: 10.0,
                GameForceRespawnTime: 30.0,
                GameSpectatorSwitchDelayTime: 120.0,
                NumEnemyVotesRequiredForKick: 4,
                NumFriendlyVotesRequiredForKick: 2,
                VoteKickBanSeconds: 1200,
                MaxIdleTime: 180,
                MinRequiredPlayersToStart: 1,
                PlayerSearchTime: 30.0,
                KickOnIdleIntermission: false,
                TimeLimit: 20,
                GoalScore: 3000,
                NumBots: 0,
                MaxBotCount: 0,
                MaxPlayers: 16,
                ForceBotsToSingleTeam: false,
                AllowUnbalancedTeams: false
            },
            mutators: {
                DisableDepots: false,
                DisableHRV: false,
                DisableHeadShots: false,
                StockLoadout: false,
                DisablePrimaries: false,
                DisableSecondaries: false,
                DisableGear: false,
                DisableTacticalGear: false,
                DisableHealthRegen: false,
                DisableElementalAmmo: false,
                HeadshotsOnly: false,
                StaminaModifier: 1.0,
                HealthModifier: 1.0
            }
        };

        for(const [section, props] of Object.entries(config)) {
            for(const [prop, value] of Object.entries(props)) {
                config[section][prop] = getProp(id, prop, value);
            }
        }

        let randomBotNames = getProp(id, 'RandomBotNames');
        if(randomBotNames === "") {
            randomBotNames = [];
        } else {
            randomBotNames = randomBotNames.split(',').map(v => v.trim());
        }
        config.properties.RandomBotNames = randomBotNames;

        return config;
    }

    function addPlaylistEntry() {
        let playlistEl = document.getElementById("playlistEntries");
        playlistEl.innerHTML += `
        <form id="playlistGameConfig${playlistEl.childElementCount+1}">
        <details class="note" open>
            <summary># ${playlistEl.childElementCount + 1}</summary>
                <label>Game Mode: ${gameModeSelectHTML}</label>
                <label>Map: ${mapSelectHTML}</label>
                <details class="tip"><summary>Game Settings</summary>
                    ${generateServerConfigForm(`playlistGameConfig${playlistEl.childElementCount+1}`)}
                </details>
        </details>
        </form>
        `;

    }

    function generatePlaylist() {
        let playlistEl = document.querySelector('#playlistEntries');
        let playlistConfig = [];
        for(const configForm of playlistEl.querySelectorAll('form')) {
            playlistConfig.push({
                Map: getProp(configForm.id, "Map"),
                GameMode: getProp(configForm.id, "GameMode"),
                Properties: generateConfig(configForm.id)
            });
        }
        return playlistConfig;
    }

    function copyConfigToClipboard(id) {
        let config = JSON.stringify(generateConfig(id), null, 2);
        navigator.clipboard.writeText(config);
        alert$.next("Copied server config to clipboard!")
    }

    function copyPlaylistToClipboard() {
        let config = JSON.stringify(generatePlaylist(), null, 2);
        navigator.clipboard.writeText(config);
        alert$.next("Copied playlist config to clipboard!")
    }

    document.getElementById('serverConfig').innerHTML = generateServerConfigForm('serverConfigForm', true);

    document.getElementById("downloadBtn").addEventListener("click", () => {
            const zip = new JSZip();

            // Example JSON data
            const serverUtilsConfig = generateConfig('serverConfigForm');
            const serverName = serverUtilsConfig.ServerName;

            // Add JSON files to ZIP
            zip.file(`FoxGame/Config/BLRevive/server_utils/configs/${serverName}.json`, JSON.stringify(serverConfigTemplate, null, 2));
            zip.file(`FoxGame/Config/BLRevive/server_utils/playlists/${serverName}.json`, JSON.stringify(generatePlaylist(), null, 2));

            // Generate ZIP and trigger download
            zip.generateAsync({ type: "blob" }).then(blob => {
                const link = document.createElement("a");
                link.href = URL.createObjectURL(blob);
                link.download = "configs.zip";
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });
        });
</script>