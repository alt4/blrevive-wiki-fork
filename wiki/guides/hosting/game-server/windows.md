As it's the game's native platform, setting up on Windows is not extremely complicated.

## Prerequisites

- Windows 7 and Server 2012 *should* be supported, but it's recommended you run Windows 10 or Server 2016 and above
    - [VC++ 2012 Update 4](https://www.microsoft.com/en-us/download/details.aspx?id=30679) *(direct link: [vcredist_x86.exe](https://download.microsoft.com/download/1/6/B/16B06F60-3B20-4FF2-B699-5E9B7962F9AE/VSU_4/vcredist_x86.exe))*
- [BL:R v3.02 client](./getting-started.md#downloading-game-client-using-depotdownloader)

## Artemis

[Artemis](https://gitlab.com/northamp/artemis-blrevive) is a Golang launcher that was originally used in the container image made to run the game. It is also available standalone for Windows (and a wide variety of other platforms).

It helps running a server with all its requirements without tainting the original game install, along with a few other features (and possibly more in the future) which are detailed in the project's README.

!!! warning "Project creep"
    Those instructions are not guaranteed to be fully up to date with Artemis' latest developments, head over to the project's README if you've got any doubts. If you've got issues with the software, [open an issue in its repository](https://gitlab.com/northamp/artemis-blrevive/-/issues).


1. Download Artemis (`artemis-windows-amd64.exe`) from its [Release page](https://gitlab.com/northamp/artemis-blrevive/-/releases). Copy it to `C:\ProgramData\blacklightre\artemis.exe`, renaming it in the process
2. Create a new file in `C:\ProgramData\blacklightre\artemis.json` with the following content, customizing it as you see fit:

    ```json
    {
        "Debug": false,
        "LogLevel": "trace",
        "TempDir": "",
        "Dirs": {
            "BLR": "C:\\ProgramData\\blacklightre\\game",
            "BLRE": "C:\\ProgramData\\blacklightre\\blre",
            "BLREConfig": "C:\\ProgramData\\blacklightre\\config",
            "Modules": "C:\\ProgramData\\blacklightre\\modules",
            "Redist": "C:\\ProgramData\\blacklightre\\redist"
        },
        "GameSettings": {
            "ConfigName": "myserver",
            "ServerName": "[Earth]+Yet+Another+BLRE+Server",
            "Password": "",
            "Playlist": "TDM",
            "Map": "",
            "GameMode": "",
            "MaxPlayers": "",
            "NumBots": "",
            "TimeLimit": "",
            "SCP": "",
            "Custom": ""
        }
    }
    ```

    !!! tip "Customizing the JSON file"
        Parameters are detailed in [Artemis' README](https://gitlab.com/northamp/artemis-blrevive#with-a-json-file).

        As you've probably guessed, content of `GameSettings` define various parameters that'll be passed to the game. You can find those parameters in greater details [here](./parameters.md).

        **One thing of note is that [you should always use `+` signs instead of spaces](https://gitlab.com/blrevive/modules/server-utils/-/commit/5c701b8046cb679870d78638d9c2041b70404bc5) in your server name if you use server-utils (you will)**

3. Configure BLRevive and server-utils as you see fit - see [Configuring BLRevive and server-utils](#configuring-blrevive-and-server-utils)
4. Start a Powershell prompt **as administrator**, enter `C:\ProgramData\blacklightre\artemis.exe`, and push Enter; the server should then start successfully!

!!! tip "Services"
    The server won't be ran as daemon or anything, it'll need to be restarted manually if it crashes/the server reboots/etc...

    It could be possible to register Artemis as a Windows service, or use things such as [NSSM](https://nssm.cc/), but it hasn't been investigated all that much. Don't hesitate if you'd like to suggest anything!

## Manually

It's recommended you use a launcher to get your server up and running, but you can also get started manually. Again, the game is assumed to reside in `C:\ProgramData\blacklightre\game`.

1. Download the latest version of BLRevive from its [Release page](https://gitlab.com/blrevive/blrevive/-/releases). You'll want both `BLRevive.dll` and `DINPUT8.dll`; place them in `C:\ProgramData\blacklightre\game\Binaries\Win32\`
2. Download the latest version of server-utils from its [Release page](https://gitlab.com/blrevive/modules/server-utils/-/releases). You'll want `server-utils.dll`; place it in `C:\ProgramData\blacklightre\game\Binaries\Win32\Modules\`
3. Download every DLL files within the `blredistributables` repository's [package registry](https://gitlab.com/northamp/blredistributables/-/packages/21551836). You should end up with `vstdlib_s.dll`, `tier0_s.dll`, `steamclient.dll`, `steam_api.dll`, and `Steam.dll`; place them in `C:\ProgramData\blacklightre\game\Binaries\Win32\`, *replacing existing files when prompted*
4. Configure BLRevive and server-utils as you see fit - see configuring [BLRevive](./blrevive-config.md) and [Server Utils](./server-utils-config.md)
5. Open a CMD prompt, enter `C:\ProgramData\blacklightre\game\Binaries\Win32\FoxGame-win32-Shipping.exe server+HeloDeck?Config=myserver?Servername=[Earth]+Yet Another+BLRE+Server?Playlist=TDM`, and push Enter; the server should then start successfully!
