!!! tip "Docker"
    Using containers is probably the easiest and most convenient/portable solution. The suggested container image, [northamp/docker-blrevive](https://gitlab.com/northamp/docker-blrevive), relies on Artemis and a custom Wine build to run the game, bringing various optimizations and hosting quality of life features such as log-forwarding to the container's stdout (which should in turns forward to the container runtime) for easier aggregation, restart upon server failure, and more.

## Prerequisites

- Any Linux distros you can run Wine/a container runtime on should be supported
  > As an example, game servers have been tested to successfully run for extended periods on Ubuntu and Flatcar

The following instructions will assume you've downloaded the game and it is stored in your **host system's** `/srv/blacklightre/game`, **making `/srv/blacklightre/game/Binaries/Win32/FoxGame-win32-Shipping.exe` a valid path**.

## Setup

<!-- Welcoming contribs - I didn't bother stealing and tweaking a custom build from Northstar to cram in the container image, to support installation outside of it lol - ALT -->

```bash
apt install wine
```

### Artemis

Using Artemis directly on your system assumes you've prepared Wine, and installed `Xvfb` (since it's also assumed you'll run the server on a headless environment).

TODO

### Manually

TODO