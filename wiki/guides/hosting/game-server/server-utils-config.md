# Server-Utils configuration

In `config/BLRevive/`, create a dir named `server_utils`.

## Configs

In `config/BLRevive/server_utils/`, create a dir named `configs` and within it a file named `default.json` (*or* that matches your server's `ServerName` parameter) with the following content:

``` json title="default.json"
{
    "info": {
        "FileName": "server_info",
        "Host": "0.0.0.0",
        "Port": 7777,
        "Serve": true,
        "ShowSpectatorTeam": false,
        "URI": "/server_info"
    },
    "mutators": {
        "DisableDepots": false,
        "DisableElementalAmmo": false,
        "DisableGear": false,
        "DisableHRV": false,
        "DisableHeadShots": false,
        "DisableHealthRegen": false,
        "DisablePrimaries": false,
        "DisableSecondaries": false,
        "DisableTacticalGear": false,
        "HeadshotsOnly": false,
        "StockLoadout": false
    },
    "properties": {
        "GameForceRespawnTime": 30.0,
        "GameRespawnTime": 7.0,
        "GameSpectatorSwitchDelayTime": 120.0,
        "GoalScore": 4500,
        "MaxIdleTime": 180.0,
        "MinRequiredPlayersToStart": 1,
        "NumEnemyVotesRequiredForKick": 4,
        "NumFriendlyVotesRequiredForKick": 2,
        "PlayerSearchTime": 50.0,
        "TimeLimit": 10,
        "VoteKickBanSeconds": 1200,
        "NumBots": 0,
        "MaxBotCount": 0,
        "MaxPlayers": 32
    }
}
```

!!! tip "Customizing the game parameters"
    Most parameters are very self-explanatory, but you can find more exhaustive details in [server-utils' README](https://gitlab.com/blrevive/modules/server-utils/#server-properties).

## Playlists - Optional

In `config/BLRevive/server_utils/`, create a dir named `playlists` and within it a file named `<your server's Playlist parameter value>.json` (i.e. if you start the server with `?Playlist=myplaylist`, it should be named `myplaylist.json`) with the following example content:

```json title="config/BLRevive/server_utils/playlists/playlist.json"
[
    {
        "Map": "HeloDeck",
        "GameMode": "DM",
        "Properties": {
            "GoalScore": 300,
            "TimeLimit": 1,
            "NumBots": 16
        }
    },
    {
        "Map": "Piledriver",
        "GameMode": "TDM",
        "Properties": {
            "GoalScore": 1234,
            "TimeLimit": 2,
            "NumBots": 2
        }
    },
    {
        "Map": "Containment",
        "GameMode": "CTF",
        "Properties": {
            "TimeLimit": 2
        }
    },
    {
        "Map": "Metro",
        "GameMode": "KC",
        "Properties": {
            "TimeLimit": 2
        }
    },
    {
        "Map": "Rig",
        "GameMode": "DOM",
        "Properties": {
            "TimeLimit": 2
        }
    },
    {
        "Map": "Safehold",
        "GameMode": "KOTH",
        "Properties": {
            "TimeLimit": 2
        }
    },
    {
        "Map": "Trench",
        "GameMode": "LMS",
        "Properties": {
            "TimeLimit": 2
        }
    },
    {
        "Map": "Vertigo",
        "GameMode": "LTS",
        "Properties": {
            "TimeLimit": 2
        }
    },
    {
        "Map": "Vortex",
        "GameMode": "SND",
        "Properties": {
            "TimeLimit": 2
        }
    }
]
```

!!! tip "Customizing the playlist"
    Add and remove maps as you see fit, and you can override pretty much any property on a per-map basis in the relevant object.

    Also, you can create a `OS_Easy` or `OS_Hard` playlist this way!

## Rules - Optional

In `config/BLRevive/server_utils/`, create a dir named `rules` and within it a file named `rules.json` (*or* that matches your [BLRevive config's `RulesFile` entry](blrevive-config.md) / the CLI arg `?blre.server.rulesfile=`) with the following example content:

```json title="config/BLRevive/server_utils/rules/rules.json"
{
    "disabledItems": [
        "12001"
    ],
    "disabledWeaponMods": {
        "40003": ["43002"]
    },
    "disabledArmorPairs": {
        "30200": [ "30300" ]
    }
}
```

## Patches - Optional (AND NOT RECOMMENDED)

!!! warning "Patches do not work when using ZCure"
    At the moment (2024-05), patches are **NOT** applied to ZCure-using players, only those using the legacy BLRevive method. It's recommended to *not* use them for now.

In `config/BLRevive/server_utils/`, create a dir named `patches` and within it a file named `patches.json` (*or* that matches your [BLRevive config's `PatchFile` entry](blrevive-config.md) / the CLI arg `?blre.server.patchfile=`) with the following example content:

```json title="config/BLRevive/server_utils/patches/patches.json"
{
    "foxgamecontent_wpn.FoxWeapon_AssaultRifle": {
        "RateOfFire": 20000
    }
}
```

!!! tip "Customizing the patch file"
    You can set pretty much any value for every weapons - it hasn't been documented yet though.
