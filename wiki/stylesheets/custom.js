// select the whole text in a code tag on double-click
//document.addEventListener('DOMContentLoaded', () => fix_code_selection());
document$.subscribe(() => fix_code_selection());

function fix_code_selection() {
    document.querySelectorAll('code').forEach(element => {
        element.addEventListener('dblclick', function() {
            const range = document.createRange();
            range.selectNodeContents(this);
            const selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        });
    });    
}
