---
title: BLRevive
author: BLRevive
---

#

<figure markdown="span">
    ![BLRevive Logo](./assets/blrevive_transparent.png#only-dark){ width="250" }
    ![BLRevive Logo](./assets/blrevive.png#only-light){ width="250" }
</figure>

BLRevive is a community project initiated to make the PC version of Blacklight: Retribution playable again after the PC servers were shutdown in 2019.

<div class="grid cards" markdown>

-   :material-controller: **Play Blacklight: Retribution on PC**

    ---

    Play Blacklight: Retribution on PC again using our community servers!

    ---

    [:octicons-arrow-right-24: Setup BLRevive](guides/user/getting-started.md)

-   :material-server: **Community Servers**

    ---

    Host your own game servers easely on any platform!

    ---

    [:octicons-arrow-right-24: Setup Game Server](guides/hosting/game-server/getting-started.md)

</div>