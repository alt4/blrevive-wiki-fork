# BLRevive Wiki

Repository for [BLRevive Wiki](https://blrevive.gitlab.io/wiki). See [improve-wiki.md](wiki\guides\wiki\improve-wiki.md) for instructions on how to contribute to it!

## issue templates

This repository uses a custom set of issue templates clearify use cases.

| Name     | Description                                             | Template                                                                    |
| -------- | ------------------------------------------------------- | --------------------------------------------------------------------------- |
| Bug      | Logic breaking issue with UI, Pipelines or scripts      | [.gitlab/issue_templates/Bug.md](/.gitlab/issue_templates/Bug.md)           |
| Request  | Request new features (like markdown plugins) or content | [.gitlab/issue_templates/Request.md](/.gitlab/issue_templates/Request.md)   |
| Revision | Rewrite or correct page content                         | [.gitlab/issue_templates/Revision.md](/.gitlab/issue_templates/Revision.md) |
